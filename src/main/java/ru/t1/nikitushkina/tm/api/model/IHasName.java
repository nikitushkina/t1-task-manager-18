package ru.t1.nikitushkina.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
