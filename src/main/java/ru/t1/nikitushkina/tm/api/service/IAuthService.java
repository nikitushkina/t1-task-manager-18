package ru.t1.nikitushkina.tm.api.service;

import ru.t1.nikitushkina.tm.model.User;

public interface IAuthService {

    User regisrty(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}
