package ru.t1.nikitushkina.tm.api.service;

import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    void clear();

    Project create(String name, String description);

    Project create(String name);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAll(Sort sort);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
