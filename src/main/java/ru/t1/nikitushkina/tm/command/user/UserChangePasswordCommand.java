package ru.t1.nikitushkina.tm.command.user;

import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = "change-user-pwd";

    public static final String DESCRIPTION = "Change user password.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}
