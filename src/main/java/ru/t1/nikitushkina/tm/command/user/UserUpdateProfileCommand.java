package ru.t1.nikitushkina.tm.command.user;

import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-update-profile";

    public static final String DESCRIPTION = "Update profile of current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

}
